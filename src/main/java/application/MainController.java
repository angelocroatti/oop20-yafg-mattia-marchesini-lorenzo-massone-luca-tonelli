/**
 * This class represent the main controller, it's used to handle the new game creation and finish
 * @author Luca Tonelli
 */
package application;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Optional;
import javafx.animation.AnimationTimer;
import javafx.stage.Stage;
import model.MediaSound;
import model.MediaSound.Sounds;

public class MainController {

    private Optional<Game> actualGame = Optional.empty();

    /**
     * Constructor method when a MainController object is instantiated it instantly start to build a new game instance.
     * @param li a list that contain the characters id 
     * @param background the string that represent the background and floor that is chosen
     * @param stage the stage to use for the game
     */

    public MainController(final List<String> li, final String background, final Stage stage) {
        this.createNewGame(li, background, stage);
    }

    /**
     * This method create a new game instance with the essentials. 
     * @param li a list that contain the characters id 
     * @param background the string that represent the background and floor that is chosen
     * @param stage the stage to use for the game
     */

    private void createNewGame(final List<String> li, final String background, final Stage stage) {
        this.actualGame = Optional.ofNullable(GameBuilder.newBuilder(stage)
                                               .background(background)
                                               .floor(background)
                                               .characters(li)
                                               .build());
    }

    /**
     * This method return an AnimationTimer with the game code if the game was correctly created. 
     * @return AnimationTimer if a game is correctly created else null
     */
    public AnimationTimer getGameStage() {
        if (actualGame.isPresent()) {
            MediaSound.Sounds.MUSIC_MENU.stopLastMediaPlayer();
            MediaSound.Sounds.MUSIC_FIGHT.playMedia(Sounds.MUSIC_FIGHT);
            return new AnimationTimer() {

                private long lastUpdate = 0;
                private Game actual = actualGame.get();
                private boolean newGameCreated = false;
                public void handle(final long currentNanoTime) {
                    if (actual.getSettings1().isRunGame() && actual.getSettings2().isRunGame()) {
                    // The first iteration our Animation Timer does
                        if (lastUpdate == 0) { 
                            lastUpdate = currentNanoTime;
                            return;
                        }

                        double t = (currentNanoTime - actual.getStartNanoTime()) / 1000000000.0;
                        double elapsedNanoSeconds = (double) currentNanoTime - lastUpdate;
                        double elapsedSeconds = elapsedNanoSeconds / 1_000_000_000.0;

                        // background image clears canvas
                        actual.getGraphicsContext().drawImage(actual.getBackground().getImage(), actual.getBackground().getX(), actual.getBackground().getY(), GameBuilder.W, GameBuilder.H);
                        actual.getGraphicsContext().drawImage(actual.getFloor().getImage(), 0, actual.getFloor().getY(), actual.getFloor().getWidth(), actual.getFloor().getHeight());

                        // we use the settings class to update the sprites coordinates and their
                        // representation on screen
                        actual.getSettings1().updateCharacterPosition(actual.getPlayer1(), actual.getPlayer2(), elapsedSeconds);
                        actual.getSettings2().updateCharacterPosition(actual.getPlayer2(), actual.getPlayer1(), elapsedSeconds);
                        try {
                            actual.getSettings1().drawPlayer(t, actual.getPlayer1());
                            actual.getSettings2().drawPlayer(t, actual.getPlayer2());
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    } else {
                        if (!this.newGameCreated) {
                            this.newGameCreated = true; 
                            MediaSound.Sounds.MUSIC_FIGHT.stopLastMediaPlayer();
                            try {
                                new Main().start(new Stage());
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                            actual.getStage().close();
                            this.stop();
                        }
                    }
                    lastUpdate = currentNanoTime;
                }
            };
        }
        return null;
    }

}
