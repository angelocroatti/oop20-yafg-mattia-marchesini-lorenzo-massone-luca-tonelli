/**
 * An implementation of {@link GameViewFactory} using JavaFX
 */

package view.factory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.util.Pair;
import javafx.beans.property.ObjectProperty;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import view.gameview.AbstractGameViewFX;
import view.gameview.GameView;

public class GameViewFactoryFX implements GameViewFactory {

    private final Stage stage; 

    public GameViewFactoryFX(final Stage stage) {
        this.stage = stage;
    }

    @Override
    public final GameView getMainMenu(final ObservableMap<String, Runnable> entries) {
        class MainMenu extends AbstractGameViewFX {

            private final VBox box;

            MainMenu() {
                super(GameView.ViewLayouts.MAIN_MENU);
                FXMLLoader loader = super.getFXMLLoader();
                try {
                    stage.setScene(new Scene(new Group((BorderPane) loader.load())));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                this.box = (VBox) loader.getNamespace().get("main_menu_vbox");
                entries.addListener((MapChangeListener.Change<? extends String, ? extends Runnable> change) -> {
                    this.updateVBox();
                });

                this.updateVBox();
           }

            public void updateVBox() {
                box.getChildren().clear();
                entries.forEach((s, r) -> {
                    Button b = new Button(s);
                    b.setOnAction(e -> r.run());
                    box.getChildren().add(b);
                });
            }
        }
            return new MainMenu();
    }

    @Override
    public final GameView getCharacterChooser(final List<Pair<String, Image>>  characters, final ObjectProperty<String> chosen) {

        class CharacterChooser extends AbstractGameViewFX {

            private final GraphicsContext context;
            private final FlowPane fPane;
            private final Button decisionButton;
            private List<Button> btList = new ArrayList<>();
            private final FXMLLoader loader;
            private Optional<Button> choiche;
            private BorderPane brPane = null;
            private final Label playerLabel;
            private int playerCounter = 0;
            private static final double CANVAS_LEN = 150.0;
            private static final double DRAW_AT = 50;

            CharacterChooser() {
                super(GameView.ViewLayouts.CHARACTER_CHOOSER);

                loader = super.getFXMLLoader();
                try {
                    brPane = (BorderPane) loader.load();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                context = ((Canvas) loader.getNamespace().get("character_chooser_canvas")).getGraphicsContext2D();
                fPane = (FlowPane) loader.getNamespace().get("character_chooser_flowpane");
                decisionButton = (Button) loader.getNamespace().get("character_chooser_button");
                playerLabel = (Label) loader.getNamespace().get("character_chooser_label");

                characters.forEach(c -> {
                    ImageView im = new ImageView(c.getValue());
                    im.setFitHeight(CANVAS_LEN);
                    im.setFitWidth(CANVAS_LEN);
                    im.setPreserveRatio(true);
                    btList.add(new Button(c.getKey(), im));
                });
                choiche = Optional.empty();

                this.updatePlayerLabel();

                btList.forEach(bt -> {
                    EventHandler<MouseEvent> eh = e -> {
                        if (!choiche.isEmpty()) {
                            this.choiche.get().setDisable(false);
                        }
                        this.choiche = Optional.of(bt);
                        bt.setDisable(true);
                        context.clearRect(0, 0, CANVAS_LEN + CANVAS_LEN, CANVAS_LEN + CANVAS_LEN);
                        context.drawImage(characters.get(Integer.parseInt(getChosen(bt))).getValue(), DRAW_AT, DRAW_AT);
                        this.decisionButton.setDisable(false);
                    };
                    bt.addEventHandler(MouseEvent.MOUSE_CLICKED, eh);
                });
                fPane.getChildren().addAll(this.btList);

                //
                EventHandler<MouseEvent> eh = e -> {
                    chosen.set(this.getChosen(this.choiche.get()));
                    this.clearCanvas();
                    this.updatePlayerLabel();
                    this.decisionButton.setDisable(true);
                    this.choiche.get().setDisable(false);
                };
                this.decisionButton.addEventHandler(MouseEvent.MOUSE_CLICKED, eh);
                this.decisionButton.setDisable(true);
                stage.setScene(new Scene(new Group(brPane)));
            }

            private String getChosen(final Button player) {
                return player.getText().split(" ")[1];
            }

            private void updatePlayerLabel() {
                this.playerCounter++;
                this.playerLabel.setText("Choose your character: Player " + this.playerCounter);
            }

            private void clearCanvas() {
                context.clearRect(0, 0, CANVAS_LEN + CANVAS_LEN, CANVAS_LEN + CANVAS_LEN);
            }
        }
        return new CharacterChooser();
    }


}
