/**
 * An interface representing attack commands. It's implemented by AttackDecorator so 
 * the different types of moves are wrapped around a basic attack 
 * @author Lorenzo Massone
 * @version 2.0
 */
package controller.attack;

public interface Attack {

    /**
     * the type of move must be implemented differently for every move and character ( theoretically... ).
     */
	public abstract void hit();
	
}
