/**
 * A class that represents an AttackController to manage the different types of attack
 * and make it simpler to call them
 * @author Lorenzo Massone
 * @author Luca Tonelli
 */
package controller.attack;

import controller.MatchController;
import javafx.scene.paint.Color;
import model.Character;
import model.MediaSound;
import model.MediaSound.Sounds;

public class AttackController {


    /**
     * This constant is used to adjust the attack collision.
     * 
     */
    private static final int SWORD_COLLISION = 40;
    private Attack la;
    private Attack sa;
    private SpecialMove sm;
    private static MatchController s;
    private Character c1;

    /**
     * The different types of Attack.
     */
    public enum Type {
        Light, Strong, Special
    }

    /**
     * The constructor of the AttackController. There should be an AttackController
     * for each players
     * 
     * @param c1
     *               is the Character who performs the attack
     * @param c2
     *               is the Character who receives the attack
     * @param s
     *               is the Settings used by c1
     */
    public AttackController(final Character c1, final Character c2, final MatchController s) {
        this.c1 = c1;
        this.la = new LightAttack(c1, c2);
        this.sa = new StrongAttack(c1, c2);
        this.sm = new SpecialMove(c1, c2);
        AttackController.s = s;
    }

    /**
     * A method to manage the different types of attack by using an enum.
     * 
     * @param type
     *                 is the type of attack that has to be called
     * @return the damage of the attack
     */
    public int attack(final Type type) {
        switch (type) {
        case Light:
            MediaSound.Sounds.L_ATTACK.playSound(Sounds.L_ATTACK);
            this.la.hit();
            break;
        case Strong:
            MediaSound.Sounds.S_ATTACK.playSound(Sounds.S_ATTACK);
            this.sa.hit();
            break;
        case Special:
            if (this.c1.getBar().getFuryCapacity() >= 100) {
                this.sm.getService(c1.getId()).cancel();
                this.sm.getService(c1.getId()).restart();
                this.c1.getBar().resetFury();
                this.c1.getBar().updateFury(0);
                this.c1.getBar().getProgressBar().getFuryRectangle().setFill(Color.BLUE);
                break;
            }
        default:
            break;
        }
        return 0;
    }

    /**
     * A method to check the collision of the character on the right side.
     * 
     * @param c1
     *               is the character that attacks
     * @param c2
     *               is the character who receives the attack
     * @return false if the characters collide, otherwise it returns true
     */
    public static boolean collisionsControlerRight(final Character c1, final Character c2) {
        if (s.determinateFirstCharacterPosition(c1.getX(), c2.getX())) {
            if ((c1.getX() + c1.getImageWidth() - SWORD_COLLISION > c2.getX() && s.collisionsControlerJump(c1, c2)
                    && s.collisionsControlerUnder(c1, c2)) && c1.isOrientation()) {
                return false;
            }
            return true;
        }
        return true;
    }

    /**
     * A method to check the collision of the character on the left side.
     * 
     * @param c1
     *               is the character that attacks
     * @param c2
     *               is the character who receives the attack
     * @return false if the characters collide, otherwise it returns true
     */
    public static boolean collisionsControlerLeft(final Character c1, final Character c2) {
        if (s.determinateFirstCharacterPosition(c2.getX(), c1.getX())) {
            if ((c1.getX() + SWORD_COLLISION < c2.getX() + c2.getImageWidth() && s.collisionsControlerJump(c1, c2)
                    && s.collisionsControlerUnder(c1, c2)) && !c1.isOrientation()) {
                return false;
            }
            return true;
        }
        return true;
    }

}
