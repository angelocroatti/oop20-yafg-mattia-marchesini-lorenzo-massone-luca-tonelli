/**
 * this class serves as a controller for the character chooser menu
 */
package controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import application.GameApplication;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.util.Pair;
import view.factory.GameViewFactory;

public class CharacterChooserController {
    private ObjectProperty<String> characterIsChosen = new SimpleObjectProperty<>();
    private final String PATH0 = "../../src/main/resources/characters/player";
    private final String PATH1 = "src/main/resources/characters/player";
    private final String PATH2 = "/idle/Idle_000.png";
    private final int NUMBER_OF_CHARACTERS = 4;
    private final int NUMBER_OF_FIGHTERS = 2;
    private List<String> fighters = new ArrayList<>();

    public CharacterChooserController(GameViewFactory factory, Stage stage) {
        List<Pair<String, Image>> list = new ArrayList<>();
        String path;
        
        for(int i = 0; i < NUMBER_OF_CHARACTERS; i++) {
            path = PATH1 + i + PATH2;
            Image img = null;
            try {
                img = new Image(new FileInputStream(path));
                list.add(new Pair<String, Image>("character " + i, img));
            } catch (FileNotFoundException e1) {
                System.out.println("Unexpected error: file " + path + " not found");
            }
            if(img==null) {
                path = PATH0 + i + PATH2;
                try {
                    img = new Image(new FileInputStream(path));
                    list.add(new Pair<String, Image>("character " + i, img));
                } catch (FileNotFoundException e1) {
                    System.out.println("Unexpected error: file " + path + " not found");
                }
            }
        }

        characterIsChosen.addListener(e -> {
            fighters.add(characterIsChosen.get());
            if(fighters.size() == NUMBER_OF_FIGHTERS) {
                List<String> backgrounds = Arrays.asList("desert", "forest", "graveyard");
                String bg = backgrounds.get(new Random().nextInt(backgrounds.size()));

                try {
                    new GameApplication(fighters, bg).start(new Stage());
                    stage.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        factory.getCharacterChooser(list, this.characterIsChosen);
    }
}
