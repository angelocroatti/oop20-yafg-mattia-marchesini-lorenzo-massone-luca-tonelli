package model;

/**
 * @author Giacomo Grassetti
 * this class handles all the audio files in the game.
 */

import java.io.File;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public abstract class MediaSound {
    //static String relPath = "../../";
    static String relPath = "";
    public static enum Sounds {
        L_ATTACK(relPath + "src/main/resources/sounds/l_attack.mp3"),
        S_ATTACK(relPath + "src/main/resources/sounds/s_attack.mp3"),
        L_HIT(relPath + "src/main/resources/sounds/oof.mp3"),
        S_HIT(relPath + "src/main/resources/sounds/oof_errape.mp3"),
        JUMP(relPath + "src/main/resources/sounds/jump.wav"),
        MUSIC_FIGHT(relPath + "src/main/resources/sounds/musicFight.mp3"),
        MUSIC_MENU(relPath + "src/main/resources/sounds/musicMenu.mp3"),
        DEATH(relPath + "src/main/resources/sounds/death.mp3"), 
        SPECIAL_MOVE_0(relPath + "src/main/resources/sounds/s_move_0.wav"), 
        SPECIAL_MOVE_1(relPath + "src/main/resources/sounds/s_move_1.wav"), 
        SPECIAL_MOVE_2(relPath + "src/main/resources/sounds/s_move_2.wav"),
        SPECIAL_MOVE_3(relPath + "src/main/resources/sounds/s_move_3.wav");

        private String sound;
        private MediaPlayer mp;
        private MediaPlayer lastService;

        /**
         * Enum Sounds costructor's
         * 
         * @param soundPath:
         *                       path of audio file
         */
        private Sounds(String soundPath) {
            this.sound = soundPath;
        }

        /**
         * get path of media
         * 
         * @param type:
         *                  type of Enum Sounds
         * @return new MediaPlayer instance
         */
        public MediaPlayer getMediaPath(Sounds type) {
            return new MediaPlayer(new Media(new File(this.sound).toURI().toString()));
        }

        /**
         * Thread to start music file
         * 
         * @param type:
         *                  type of Enum Sounds
         */
        public void playMedia(Sounds type) {
            Service<Void> playMedia = new Service<Void>() {
                @Override
                protected Task<Void> createTask() {
                    return new Task<Void>() {
                        protected Void call() throws Exception {
                            mp = new MediaPlayer(new Media(new File(sound).toURI().toString()));
                            lastService = mp;
                            mp.setVolume(0.1);
                            mp.play();
                            return null;
                        }
                    };
                }
            };
            playMedia.restart();
        }

        /**
         * Thread to start audio clip
         * 
         * @param type:
         *                  type of Enum Sounds
         */
        public void playSound(Sounds type) {
            Service<Void> playSound = new Service<Void>() {
                @Override
                protected Task<Void> createTask() {
                    return new Task<Void>() {
                        protected Void call() throws Exception {
                            AudioClip ac = new AudioClip(new File(sound).toURI().toString());
                            ac.play();
                            return null;
                        }
                    };
                }
            };
            playSound.restart();
        }

        /**
         * Stop last MediaPlayer object started
         */
        public void stopLastMediaPlayer() throws NullPointerException {
            this.lastService.stop();
        }

    }

}
