/**
 * This class represents a personage consisting of an image,
 * a position (x,y), height and width.
 * Getter and Setter included
 * @author Giacomo Grassetti
 * @author Lorenzo Massone
 * @author Luca Tonelli
 * @version 1.1 
 * 
 */

package model;

import java.io.FileNotFoundException;
import javafx.scene.image.Image;
import view.gamefield.Sprite;

public class Character extends Sprite {

    private Indicator bar;

    private boolean up;
    private boolean down;
    private boolean left;
    private boolean right;
    private boolean lightAttacking;
    private boolean strongAttacking;
    private boolean jump = false;

    private boolean furyFull = true;
    private boolean player1;
    private double imageWidth;
    private double imageHeight;

    private boolean specialMove = false;
    private boolean orientation = true;

    private final double defSpeedJump = 6;
    private double speedJump = this.defSpeedJump;

    private double speed = 350; // pixels/second
    private final double defSpeed = 350;
    private final String id;

    public Character(final Image image, final boolean player1, final String id) throws FileNotFoundException {
        super(0, 0, image);

        this.id = id;
        this.player1 = player1;

        this.setImageHeight(this.getImage().getHeight());
        this.setImageWidth(this.getImage().getWidth());

        this.getImageView().setPreserveRatio(true);

        this.getImageView().setFitHeight(400);
        this.getImageView().setFitWidth(200);

        this.bar = new Indicator(player1);

        if (!player1) {
            this.orientation = false;
        }
    }

    /**
     * @return Indicator bar
     */
    public Indicator getBar() {
        return this.bar;
    }

    /**
     * @return jump speed of the Character
     */
    public double getJumpSpeed() {
        return this.speedJump;
    }

    /**
     * Sets the JumpSpeed of the character.
     * 
     * @param value
     *                  is the JumpSpeed that has to be set
     */
    public void setJumpSpeed(final double value) {
        this.speedJump = value;
    }

    /**
     * A method to return the current speed of the Character.
     * 
     * @return the current speed of the Character
     */
    public synchronized double getSpeed() {
        return this.speed;
    }

    /**
     * A method to get the default speed of the Character.
     * 
     * @return the default speed of the Character
     */
    public synchronized double getDefaultSpeed() {
        return this.defSpeed;
    }

    /**
     * A method to set character speed of movement.
     * 
     * @param d
     *              is the speed that has to be set instead of the current one
     */
    public synchronized void setSpeed(double d) {
        this.speed = d;
    }

    /**
     * A method to check if the current direction of the Character is Up.
     * 
     * @return true if is directed Up, otherwhise false.
     */
    public final boolean isUp() {
        return up;
    }

    /**
     * A method to set the directional flag "Up" of the Character.
     * 
     * @param up
     *               is the boolean that the up flag has to be set to.
     */
    public final void setUp(boolean up) {
        this.up = up;
    }

    /**
     * A method to check if the current direction of the Character is Down.
     * 
     * @return true if the Character is directed Down, otherwhise false.
     */
    public final boolean isDown() {
        return down;
    }

    /**
     * A method to set the directional flag "Dowm" of the Character.
     * 
     * @param down
     *                 is the boolean that the down flag has to be set to.
     */
    public final void setDown(final boolean down) {
        this.down = down;
    }

    /**
     * A method to check if the current direction of the Character is Left.
     * 
     * @return true if the Character is directed Left, otherwhise false.
     */
    public final boolean isLeft() {
        return left;
    }

    /**
     * A method to set the directional flag "Left" of the Character.
     * 
     * @param left
     *                 the Character is the boolean that the up flag has to be set
     *                 to.
     */
    public final void setLeft(final boolean left) {
        this.left = left;
    }

    /**
     * A method to check if the current direction of the Character is Right.
     * 
     * @return true if is directed Right, otherwhise false.
     */
    public final boolean isRight() {
        return right;
    }

    /**
     * A method to set the directional flag "Right" of the Character.
     * 
     * @param right
     *                  is the boolean that the right flag has to be set to.
     */
    public final void setRight(final boolean right) {
        this.right = right;
    }

    /**
     * A method to check if the Character is doing the LightAttack.
     * 
     * @return true if the Character is doing that, otherwhise false.
     */
    public final boolean isLightAttacking() {
        return lightAttacking;
    }

    /**
     * A method to set the offensive flag "lightAttacking" of the Character.
     * 
     * @param attack
     *                   is the boolean that the lightAttacking flag has to be set
     *                   to.
     */
    public final void setLightAttacking(final boolean attack) {
        this.lightAttacking = attack;
    }

    /**
     * A method to check if the Character is doing the StrongAttack.
     * 
     * @return true if the Character is doing that, otherwhise false.
     */
    public final boolean isStrongAttacking() {
        return strongAttacking;
    }

    /**
     * A method to set the offensive flag "strongAttacking" of the Character.
     * 
     * @param attack
     *                   is the boolean that the strongAttacking flag has to be set
     *                   to.
     */
    public final void setStrongAttacking(final boolean attack) {
        this.strongAttacking = attack;
    }

    /**
     * A method to set the directional flag "up" of the Character.
     * 
     * @param up
     *               is the boolean that the up flag has to be set to.
     */
    public final void setJumping(final boolean up) {
        this.up = up;
    }

    /**
     * A method to check if the current direction of the Character is Jumping.
     * 
     * @return true if the Character is jumping, otherwhise false.
     */
    public final boolean getJumpStatement() {
        return this.jump;
    }

    /**
     * A method to set the directional flag "Jump" of the Character.
     * 
     * @param value
     *               is the boolean that the up flag has to be set to.
     */
    public final void setJumpStatement(final boolean value) {
        this.jump = value;
    }

    /**
     * A method to check if the Character is doing the SpecialMove.
     * 
     * @return true if the Character is doing that, otherwhise false.
     */
    public boolean isSpecialMoving() {
        return specialMove;
    }

    /**
     * A method to set the offensive flag "specialAttack" of the Character.
     * 
     * @param specialMove
     *                        is the boolean that the specialMove flag has to be set
     *                        to.
     */
    public void setSpecialAttack(final boolean specialMove) {
        this.specialMove = specialMove;
    }

    /**
     * A method to check if the the Fury indicator is full.
     * 
     * @return true if it is , otherwhise false.
     */
    public boolean isFuryFull() {
        return furyFull;
    }

    /**
     * A method to set the furyfull flag.
     * 
     * @param furyFull
     *                     is the boolean that the furyFull flag has to be set to.
     */
    public void setFuryFull(final boolean furyFull) {
        this.furyFull = furyFull;
    }

    /**
     * A method to check the current orientation of the Character.
     * 
     * @return true if is directed Right, otherwhise false.
     */
    public final boolean isOrientation() {
        return orientation;
    }

    /**
     * A method to set the orientation of the character.
     * 
     * @param orientation
     *                        is the value the flag orientation has to be set to
     */
    public final void setOrientation(final boolean orientation) {
        this.orientation = orientation;
    }

    /**
     * A method to get the current Id of the Character.
     * 
     * @return the ID of the Character
     */
    public String getId() {
        return this.id;
    }

    /**
     * A method to check if the current Character is the Player1.
     * 
     * @return true if is directed Down, otherwhise false.
     */
    public boolean isPlayer1() {
        return player1;
    }

    /**
     * A method to set the Default Jump Speed of the Character.
     * 
     * @return def_speed_jump
     */
    public double getDefSpeedJump() {
        return this.defSpeedJump;
    }

    public final void setImageWidth(final double imageWidth) {
        this.imageWidth = imageWidth;
    }

    public final void setImageHeight(final double imageHeight) {
        this.imageHeight = imageHeight;
    }

    public final double getImageWidth() {
        return imageWidth;
    }

    public final double getImageHeight() {
        return imageHeight;
    }

}
