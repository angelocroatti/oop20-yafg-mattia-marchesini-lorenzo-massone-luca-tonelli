/**
 * This class represent the Game Background image.
 */
package model;

import javafx.scene.image.Image;
import view.gamefield.Sprite;

public class Background extends Sprite {

    public Background(final double x, final double y, final Image image) {
        super(x, y, image);
    }

}
